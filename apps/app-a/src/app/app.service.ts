import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class AppService {
  constructor(private readonly httpService: HttpService) {}

  getData(): { message: string } {
    console.log('Begin app A test');
    return { message: 'Hello API' };
  }

  async getMockData() {
    console.log('Begin app A grafana test');
    const response = await lastValueFrom(
      this.httpService.get('http://localhost:3002/api/conf')
    );
    return response.data;
  }
}

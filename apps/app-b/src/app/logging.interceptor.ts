import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger,
} from '@nestjs/common';
import { Observable, firstValueFrom } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { HttpService } from '@nestjs/axios';
import { AxiosError } from 'axios';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(private readonly httpService: HttpService) {}
  private readonly logger = new Logger(LoggingInterceptor.name);

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    if (context.getType() === 'http') {
      return this.logHttpCall(context, next);
    }
  }

  private logHttpCall(context: ExecutionContext, next: CallHandler) {
    const request = context.switchToHttp().getRequest();
    const userAgent = request.get('user-agent') || '';
    const { ip, method, path: url } = request;
    const correlationKey = uuidv4();
    const userId = request.user?.userId;

    this.logger.log(
      `[${correlationKey}] ${method} ${url} ${userId} ${userAgent} ${ip}: ${
        context.getClass().name
      } ${context.getHandler().name}`
    );

    return next.handle().pipe(
      tap(async (responseBody: any) => {
        const response = context.switchToHttp().getResponse();

        const { statusCode } = response;
        const contentLength = response.get('content-length');
        // const logData = responseBody;
        const logData = `[${correlationKey}] ${method} ${url} ${statusCode} ${contentLength}: ${JSON.stringify(
          responseBody
        )}`;
        this.logger.log(logData);
        await this.pushToGrafana(logData);
      })
    );
  }

  private async pushToGrafana(body: string) {
    const logs = {
      streams: [
        {
          stream: {
            test_service: 'api-service',
            service_name: 'test-loki-grafana-local',
          },
          values: [[(Date.now() * 1e6).toString(), body]],
        },
      ],
    };
    try {
      await firstValueFrom(
        this.httpService
          .post('http://localhost:3100/loki/api/v1/push', logs, {})
          .pipe(
            catchError((error: AxiosError) => {
              console.log(error);
              this.logger.error(error.response.data);
              throw error;
            })
          )
      );
    } catch (error) {
      this.logger.error(error.toString());
    }
  }
}

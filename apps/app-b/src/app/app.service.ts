import { Injectable } from '@nestjs/common';
import { getJSDocDeprecatedTag } from 'typescript';
import { v4 as uuidv4 } from 'uuid'; // Import เฉพาะ v4 (UUID version 4)

@Injectable()
export class AppService {
  getData(): string {
    return 'Hello World!';
  }

  getHello(): string {
    return 'Hello World!';
  }

  testResponse() {
    console.log('Begin test response');
    return {
      config_id: '123456',
      config_name: 'MyAppConfig',
      config_settings: {
        key1: 'value1',
        key2: 'value2',
        key3: 'value3',
      },
      created_at: '2024-04-18T12:00:00Z',
      updated_at: '2024-04-18T12:30:00Z',
    };
  }

  testGenerateUid() {
    // สร้าง UUID version 4
    const generatedUid = uuidv4();

    return { uid: generatedUid };
  }
}

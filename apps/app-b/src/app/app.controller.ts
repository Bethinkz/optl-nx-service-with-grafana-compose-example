import { Controller, Get } from '@nestjs/common';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/conf')
  getConfig() {
    return this.appService.testResponse();
  }

  @Get('/uid')
  getUid() {
    return this.appService.testGenerateUid();
  }
}
